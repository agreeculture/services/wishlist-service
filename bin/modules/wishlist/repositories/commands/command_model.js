

const generalWishlist = () => {
  const model = {
    id:'',
    buyerId:'',
    productId:'',
    createdAt:'',
    updatedAt:'',
  };
  return model;
};

module.exports = {
  generalWishlist: generalWishlist
};
