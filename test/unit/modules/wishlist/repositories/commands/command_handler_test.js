

const commandHandler = require('../../../../../../bin/modules/wishlist/repositories/commands/command_handler');
const domain = require('../../../../../../bin/modules/wishlist/repositories/commands/domain');
const validator = require('../../../../../../bin/modules/wishlist/utils/validator');
const wrapper = require('../../../../../../bin/helpers/utils/wrapper');
const EventEmitter = require('events').EventEmitter;
const sinon = require('sinon');
const assert = require('assert');

describe('postOneWishlist', () => {

  let queryResult = {
    err: null,
    data: {
      id:`1`,
      buyerId:`1`,
      productId:'1',
    },
    message: 'Your Request Has Been Processed',
    code: 200
  };

  let payload = {
    id:`1`,
    buyerId:`1`,
    productId:'1',
  };

  it('Should return product object', async () => {

    sinon.stub(domain.prototype, 'addNewWishlist').returns(queryResult);
    sinon.stub(validator, 'ifExistWishlist').returns(queryResult);

    const rs = await commandHandler.postOneWishlist(payload);

    assert.equal(rs.err, null);
    assert.equal(rs.code, 200);
    assert.equal(rs.data.buyerId, '1');

    domain.prototype.addNewWishlist.restore();
    validator.ifExistWishlist.restore();

  });

  // it('Should return error event store mockup object', async () => {

  //     let obj = {
  //         err: new Error('whoops!'),
  //         data: null,
  //         message: 'Your Request Can not Processed',
  //         code: 500
  //     };

  //     const spy = sinon.spy();
  //     const emitter = new EventEmitter;

  //     emitter.on('error', spy);
  //     emitter.emit('error')
  //     sinon.stub(domain.prototype, 'addNewMockup').returns(obj);
  //     sinon.stub(domain.prototype, 'publishNewMockup').returns(obj);
  //     sinon.stub(validator, 'ifExistMockup').returns(queryResult);

  //     const rs = await commandHandler.postOneMockup(payload);

  //     // console.log
  //     // assert.equal(rs.err, null);
  //     assert.equal(rs.code, 500);

  //     sinon.assert.calledOnce(spy);

  //     domain.prototype.addNewMockup.restore();
  //     domain.prototype.publishNewMockup.restore();
  //     validator.ifExistMockup.restore();

  // });

});
